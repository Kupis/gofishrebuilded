﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GoFish
{
    class Card
    {
        public Card(Suits suit, Values value)
        {
            Suit = suit;
            Value = value;
        }

        public Suits Suit { get; set; }
        public Values Value { get; set; }

        public override string ToString()
        {
            return Value.ToString() + " of " + Suit.ToString();
        }

        public static bool DoesCardMatch(Card cardToCheck, Suits suit)
        {
            if (cardToCheck.Suit == suit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool DoesCardMatch(Card cardToCheck, Values value)
        {
            if (cardToCheck.Value == value)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string[] names1 = new string[]
            {"", "ace", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king"};
        private static string[] names2AndMore = new string[]
            {"", "aces", "doubles", "three", "fours", "fives", "sixes", "sevens", "eights", "nines", "tens", "jacks", "queens", "kings"};

        public static string Plural(Values value, int count)
        {
            if (count == 1)
                return names1[(int)value];
            else
                return names2AndMore[(int)value];
        }

        public string Name
        {
            get
            {
                return Value.ToString() + " " + Suit.ToString();
            }
        }
    }
}
